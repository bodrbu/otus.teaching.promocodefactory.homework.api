﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace SignalRClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
              .WithUrl("https://localhost:5001/hubs/customershub")
              .WithAutomaticReconnect()
              .Build();

            await connection.StartAsync();

            connection.On("CustomerChanged",
                (string customerId) => { Console.WriteLine($"Customer with Id {customerId} has been changed"); });

            var customers =
                await connection.InvokeAsync<List<CustomerShortResponse>>("GetCustomersAsync");

            customers.ForEach(customer =>
            {
            Console.WriteLine($"Customer received: {customer.FirstName};");
            });

            var customer =
                await connection.InvokeAsync<CustomerResponse>("GetCustomerAsync",
                    "a6c8c6b1-4349-45b0-ab31-244740aaf0f0");

            var editResult = await connection.InvokeAsync<string>("EditCustomersAsync",
                "a6c8c6b1-4349-45b0-ab31-244740aaf0f0", new
                {
                    FirstName = "Fedor",
                    LastName = "Petrov",
                    Email = "test@test.com",
                    PreferenceIds = new List<Guid>()
                });

            var deleteResult = await connection.InvokeAsync<string>("DeleteCustomerAsync",
                "a6c8c6b1-4349-45b0-ab31-244740aaf0f0");


            Console.ReadLine();
            await connection.StopAsync();
        }
    }
}
