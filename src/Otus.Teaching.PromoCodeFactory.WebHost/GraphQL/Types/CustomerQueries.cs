﻿using HotChocolate;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL.Types
{
    public class CustomerQueries
    {
        public Task<IEnumerable<Customer>> GetCustomers([Service] IRepository<Customer> customersRepository)
        {
            return customersRepository.GetAllAsync();
        }

        public Task<Customer> GetCustomer(
            [Service] IRepository<Customer> customersRepository,
            Guid id
            )
        {
            return customersRepository.GetByIdAsync(id);
        }
    }
}
